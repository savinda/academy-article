package id.investree.academy.article.controller.user;

import id.investree.academy.article.exceptions.DataNotFoundException;
import id.investree.academy.article.exceptions.DuplicateUsernameException;
import id.investree.academy.article.helper.response.ResponseHelper;
import id.investree.academy.article.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.*;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/user")
    public ResponseHelper create(@Valid @RequestBody UserRegistrationForm user) throws DataNotFoundException, DuplicateUsernameException {
        return new ResponseHelper(userService.createUser(user), HttpStatus.CREATED);
    }

    @GetMapping("/users")
    public ResponseHelper index() {
        return new ResponseHelper(userService.getUsers(), HttpStatus.OK);
    }

    @GetMapping("/user/{id}")
    public ResponseHelper show(@PathVariable(name = "id") String id) throws DataNotFoundException {
        User result;
        try {
            Long userId = Long.parseLong(id);
            result = userService.showUser(userId);
        } catch (NumberFormatException $e) {
            result = userService.showUser(id);
        }
        return new ResponseHelper(result, HttpStatus.OK);
    }

    @GetMapping("/user/{id}/posts")
    public ResponseHelper showPosts(@PathVariable(name = "id") Long id) throws DataNotFoundException {
        return new ResponseHelper(userService.getUserPosts(id), HttpStatus.OK);
    }

    @PostMapping("/user/reset-password")
    public ResponseHelper resetPassword(@Valid @RequestBody ResetPasswordForm resetPasswordForm) throws UsernameNotFoundException {
        userService.passwordReset(resetPasswordForm);
        return new ResponseHelper(HttpStatus.OK);
    }

    @PutMapping("user/{id}")
    public ResponseHelper updateProfile(@PathVariable(name = "id") Long id, @Valid @RequestBody UserProfileForm user) throws DataNotFoundException {
        return new ResponseHelper(userService.updateUser(id, user), HttpStatus.OK);
    }

    @Value("${security.jwt.client-id}")
    private String clientId;

    @Value("${security.jwt.client-secret}")
    private String clientSecret;

    @Value("${app.base-url}")
    private String baseUrl;

    @PostMapping("/user/login")
    public ResponseHelper login(@Valid @RequestBody UserLoginForm user) {
        RestTemplate restTemplate = new RestTemplate();

        String credentials = clientId+":"+clientSecret;
        String encodedCredentials = new String(Base64.getEncoder().encode(credentials.getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "Basic " + encodedCredentials);

        HttpEntity<String> request = new HttpEntity<>(headers);

        String accessTokenUrl = baseUrl + "/oauth/token";
        accessTokenUrl += "?username=" + user.getUsername();
        accessTokenUrl += "&password=" + user.getPassword();
        accessTokenUrl += "&grant_type=" + user.getGrantType();

        try {
            Object response = restTemplate.postForObject(accessTokenUrl, request, Object.class);
            return new ResponseHelper(response, HttpStatus.OK);
        } catch (HttpClientErrorException ex) {
            return new ResponseHelper("Bad Credentials", null, HttpStatus.UNAUTHORIZED, "");
        }
    }
}
