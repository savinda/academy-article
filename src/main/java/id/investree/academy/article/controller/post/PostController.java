package id.investree.academy.article.controller.post;

import id.investree.academy.article.entity.PostEntity;
import id.investree.academy.article.exceptions.DataNotFoundException;
import id.investree.academy.article.helper.response.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class PostController {

    @Autowired
    private PostService postService;

    @PostMapping("/post")
    @PreAuthorize("hasAuthority('ADMIN_ROLE') or hasAuthority('USER_ROLE')")
    public ResponseHelper create(@Valid @RequestBody PostEntity postEntity) {
        return new ResponseHelper(postService.createPost(postEntity), HttpStatus.CREATED);
    }

    @GetMapping("/posts")
    public ResponseHelper index(@RequestParam(value = "contentSize", defaultValue = "100") int contentSize,
                                @RequestParam(value = "page", defaultValue = "1") int page,
                                @RequestParam(value = "perPage", defaultValue = "10") int perPage) {
        return new ResponseHelper(postService.getPosts(contentSize, page, perPage), HttpStatus.OK);
    }

    @GetMapping("/post/{id}")
    public ResponseHelper show(@PathVariable(name = "id") Long id) throws DataNotFoundException {
        return new ResponseHelper(postService.getPost(id), HttpStatus.OK);
    }

    @PutMapping("/post/{id}")
    public ResponseHelper update(@PathVariable(name = "id") Long id,
                                 @Valid @RequestBody PostEntity postEntity) throws DataNotFoundException {
        return new ResponseHelper(postService.updatePost(id, postEntity), HttpStatus.OK);
    }

    @DeleteMapping("/post/{id}")
    public ResponseHelper delete(@PathVariable(name = "id") Long id) throws DataNotFoundException {
        postService.deletePost(id);
        return new ResponseHelper(HttpStatus.OK);
    }
}
