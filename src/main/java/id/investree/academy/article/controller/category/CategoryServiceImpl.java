package id.investree.academy.article.controller.category;

import id.investree.academy.article.entity.CategoryEntity;
import id.investree.academy.article.exceptions.DataNotFoundException;
import id.investree.academy.article.model.Category;
import id.investree.academy.article.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category createCategory(CategoryEntity categoryEntity) {
        categoryRepository.save(categoryEntity);
        return new Category(categoryEntity.getId(), categoryEntity.getName());
    }

    @Override
    public List<Category> getCategories() {
        List<CategoryEntity> categories = categoryRepository.findByDeletedAtIsNull();
        List<Category> categoryModel = new ArrayList<>();
        for (CategoryEntity category : categories) {
            categoryModel.add(new Category(category.getId(), category.getName()));
        }
        return categoryModel;
    }

    @Override
    public Category getCategory(Long id) throws DataNotFoundException {
        CategoryEntity categoryEntity = categoryRepository.findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new DataNotFoundException(id));
        return new Category(categoryEntity.getId(), categoryEntity.getName());
    }

    @Override
    public Category updateCategory(Long id, CategoryEntity category) throws DataNotFoundException {
        CategoryEntity categoryEntity = categoryRepository.findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new DataNotFoundException(id));
        categoryEntity.setName(category.getName());
        categoryRepository.save(categoryEntity);
        return new Category(categoryEntity.getId(), categoryEntity.getName());
    }

    @Override
    public void deleteCategory(Long id) throws DataNotFoundException {
        CategoryEntity result = categoryRepository.findByIdAndDeletedAtIsNull(id).orElseThrow(() -> new DataNotFoundException(id));
        result.setDeletedAt(new Date());
        categoryRepository.save(result);
    }

}
