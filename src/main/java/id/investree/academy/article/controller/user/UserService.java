package id.investree.academy.article.controller.user;

import id.investree.academy.article.entity.UserEntity;
import id.investree.academy.article.exceptions.DataNotFoundException;
import id.investree.academy.article.exceptions.DuplicateUsernameException;
import id.investree.academy.article.model.*;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public interface UserService {
    User createUser(UserRegistrationForm user) throws DataNotFoundException, DuplicateUsernameException;

    List<User> getUsers();

    User showUser(Long id) throws DataNotFoundException;

    User showUser(String id) throws DataNotFoundException;

    List<Post> getUserPosts(Long id) throws DataNotFoundException;

    void passwordReset(ResetPasswordForm username) throws UsernameNotFoundException;

    User updateUser(Long id, UserProfileForm user) throws DataNotFoundException;
}
