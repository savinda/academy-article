package id.investree.academy.article.controller.post;

import id.investree.academy.article.entity.PostEntity;
import id.investree.academy.article.exceptions.DataNotFoundException;
import id.investree.academy.article.helper.authenticated.user.AuthenticatedUser;
import id.investree.academy.article.model.Post;
import id.investree.academy.article.model.User;
import id.investree.academy.article.repository.CategoryRepository;
import id.investree.academy.article.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private AuthenticatedUser authenticatedUser;

    @Override
    public Post createPost(PostEntity postEntity) {
        postEntity.setCreatedBy(authenticatedUser.getUserId());
        postEntity.setUpdatedBy(authenticatedUser.getUserId());
        return savePost(postEntity);
    }

    private Post savePost(PostEntity postEntity) {
        List<Long> categories = new ArrayList<>();
        postEntity.getCategories().forEach(category -> categories.add(category.getId()));
        postEntity.setCategories(categoryRepository.findAllById(categories));
        postRepository.save(postEntity);
        Post model = new Post(postEntity.getTitle(), postEntity.getContent(), postEntity.getCreatedAt());
        model.setId(postEntity.getId());
        model.setCategories(postEntity.getCategories());
        return model;
    }

    @Override
    public List<Post> getPosts(int contentSize, int pageNumber, int perPage) {
        if (pageNumber < 1) return new ArrayList<>();
        Pageable page = PageRequest.of(pageNumber - 1, perPage);
        List<PostEntity> posts = postRepository.findByDeletedAtIsNull(page);
        List<Post> postModel = new ArrayList<>();
        for (PostEntity post : posts) {
            Post model = new Post(post.getTitle(), post.getTruncatedString(contentSize), post.getCreatedAt());
            model.setId(post.getId());
            model.setCategories(post.getCategories());
            model.setCreatedBy(getUser(post));
            postModel.add(model);
        }
        return postModel;
    }

    @Override
    public Post getPost(Long id) throws DataNotFoundException {
        PostEntity result = postRepository.findByIdAndDeletedAtIsNull(id).orElseThrow(() -> new DataNotFoundException(id));
        Post model = new Post(result.getTitle(), result.getContent(), result.getCreatedAt());
        model.setId(result.getId());
        model.setCreatedBy(getUser(result));
        model.setCategories(result.getCategories());
        return model;
    }

    @Override
    public Post updatePost(Long id, PostEntity postEntity) throws DataNotFoundException {
        PostEntity result = postRepository.findByIdAndDeletedAtIsNull(id).orElseThrow(() -> new DataNotFoundException(id));
        result.setTitle(postEntity.getTitle());
        result.setContent(postEntity.getContent());
        result.setCategories(postEntity.getCategories());
        result.setUpdatedBy(authenticatedUser.getUserId());
        return savePost(result);
    }

    @Override
    public void deletePost(Long id) throws DataNotFoundException {
        PostEntity result = postRepository.findByIdAndDeletedAtIsNull(id).orElseThrow(() -> new DataNotFoundException(id));
        result.setDeletedAt(new Date());
        result.setDeletedBy(authenticatedUser.getUserId());
        postRepository.save(result);
    }

    private User getUser(PostEntity post) {
        User user = new User(post.getUser().getUsername(), post.getUser().getFullName());
        user.setEmail(post.getUser().getEmail());
        user.setTotalPosts(post.getUser().getPosts().size());
        return user;
    }
}
