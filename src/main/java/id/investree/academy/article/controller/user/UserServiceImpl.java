package id.investree.academy.article.controller.user;

import id.investree.academy.article.entity.PostEntity;
import id.investree.academy.article.entity.RoleEntity;
import id.investree.academy.article.entity.UserEntity;
import id.investree.academy.article.exceptions.DataNotFoundException;
import id.investree.academy.article.exceptions.DuplicateUsernameException;
import id.investree.academy.article.model.*;
import id.investree.academy.article.repository.RoleRepository;
import id.investree.academy.article.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User createUser(UserRegistrationForm user) throws DataNotFoundException, DuplicateUsernameException {
        if (userRepository.existsByUsername(user.getUsername()))
            throw new DuplicateUsernameException();
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(user.getUsername());
        userEntity.setFullName(user.getFullName());
        userEntity.setEmail(user.getEmail());
        userEntity.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userEntity.setRoles(defaultRole());
        userRepository.save(userEntity);
        User userModel = new User(userEntity.getUsername(), userEntity.getFullName());
        userModel.setRoles(userEntity.getRoles());
        userModel.setEmail(userEntity.getEmail());
        return userModel(userEntity);
    }

    @Override
    public List<User> getUsers() {
        List<UserEntity> users = userRepository.findAll();
        List<User> userModel = new ArrayList<>();
        for (UserEntity user : users) {
//            User model = new User(user.getUsername(), user.getFullName());
//            model.setId(user.getId());
//            model.setEmail(user.getEmail());
//            model.setTotalPosts(user.getPosts().size());
            userModel.add(userModel(user));
        }
        return userModel;
    }

    @Override
    public User showUser(Long id) throws DataNotFoundException {
        UserEntity user = userRepository.findById(id).orElseThrow(() -> new DataNotFoundException(id));
        return userModel(user);
    }

    @Override
    public User showUser(String id) throws DataNotFoundException {
        UserEntity user = userRepository.findByUsername(id);
        if (user == null) throw new DataNotFoundException(id);
        return userModel(user);
    }

    @Override
    public List<Post> getUserPosts(Long id) throws DataNotFoundException {
        UserEntity user = userRepository.findById(id).orElseThrow(() -> new DataNotFoundException(id));
        List<Post> posts = new ArrayList<>();
        for (PostEntity post : user.getPosts()){
            Post postModel = new Post();
            postModel.setId(post.getId());
            postModel.setTitle(post.getTitle());
            postModel.setCreatedAt(post.getCreatedAt());
            postModel.setCategories(post.getCategories());
            posts.add(postModel);
        }
        return posts;
    }

    @Override
    public void passwordReset(ResetPasswordForm userData) {
        UserEntity user = userRepository.findByUsername(userData.getUsername());
        if (user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", userData.getUsername()));
        }
        user.setPassword(bCryptPasswordEncoder.encode(userData.getPassword()));
        userRepository.save(user);
    }

    @Override
    public User updateUser(Long id, UserProfileForm user) throws DataNotFoundException {
        UserEntity userEntity = userRepository.findById(id).orElseThrow(() -> new DataNotFoundException(id));
        if (user.getFullName() != null) userEntity.setFullName(user.getFullName());
        if (user.getEmail() != null) userEntity.setEmail(user.getEmail());
        userRepository.save(userEntity);
        return userModel(userEntity);
    }

    private List<RoleEntity> defaultRole() throws DataNotFoundException {
        RoleEntity role = roleRepository.findById(1L).orElseThrow(() -> new DataNotFoundException(1L));
        List<RoleEntity> defaultRole = new ArrayList<>();
        defaultRole.add(role);
        return defaultRole;
    }

    private User userModel(UserEntity userEntity) {
        User userModel = new User(userEntity.getUsername(), userEntity.getFullName());
        userModel.setId(userEntity.getId());
        userModel.setEmail(userEntity.getEmail());
        userModel.setRoles(userEntity.getRoles());
        userModel.setTotalPosts(userEntity.getPosts().size());
        return userModel;
    }
}
