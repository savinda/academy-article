package id.investree.academy.article.controller.post;

import id.investree.academy.article.entity.PostEntity;
import id.investree.academy.article.exceptions.DataNotFoundException;
import id.investree.academy.article.model.Post;

import java.util.List;

public interface PostService {
    Post createPost(PostEntity postEntity);
    List<Post> getPosts(int contentSize, int perPage, int size);
    Post getPost(Long id) throws DataNotFoundException;
    Post updatePost(Long id, PostEntity postEntity) throws DataNotFoundException;
    void deletePost(Long id) throws DataNotFoundException;
}
