package id.investree.academy.article.controller.category;

import id.investree.academy.article.entity.CategoryEntity;
import id.investree.academy.article.exceptions.DataNotFoundException;
import id.investree.academy.article.model.Category;

import java.util.List;

public interface CategoryService {
    Category createCategory(CategoryEntity category);
    List<Category> getCategories();
    Category getCategory(Long id) throws DataNotFoundException;
    Category updateCategory(Long id, CategoryEntity category) throws DataNotFoundException;
    void deleteCategory(Long id) throws DataNotFoundException;
}
