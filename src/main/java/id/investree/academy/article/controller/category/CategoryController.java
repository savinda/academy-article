package id.investree.academy.article.controller.category;

import id.investree.academy.article.entity.CategoryEntity;
import id.investree.academy.article.exceptions.DataNotFoundException;
import id.investree.academy.article.helper.response.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping("/category")
    public ResponseHelper create(@Valid @RequestBody CategoryEntity categoryEntity) {
        return new ResponseHelper(categoryService.createCategory(categoryEntity), HttpStatus.CREATED);
    }

    @GetMapping("/categories")
    public ResponseHelper index() {
        return new ResponseHelper(categoryService.getCategories(), HttpStatus.OK);
    }

    @GetMapping("/category/{id}")
    public ResponseHelper show(@PathVariable(name = "id") Long id) throws DataNotFoundException {
        return new ResponseHelper(categoryService.getCategory(id), HttpStatus.OK);
    }

    @PutMapping("/category/{id}")
    public ResponseHelper update(@PathVariable(name = "id") Long id, @Valid @RequestBody CategoryEntity categoryEntity) throws DataNotFoundException {
        return new ResponseHelper(categoryService.updateCategory(id, categoryEntity), HttpStatus.OK);
    }

    @DeleteMapping("/category/{id}")
    public ResponseHelper delete(@PathVariable(name = "id") Long id) throws DataNotFoundException {
        categoryService.deleteCategory(id);
        return new ResponseHelper(HttpStatus.OK);
    }
}
