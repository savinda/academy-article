package id.investree.academy.article.exceptions;

import id.investree.academy.article.helper.response.ResponseHelper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.DefaultThrowableAnalyzer;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.web.util.ThrowableAnalyzer;
import org.springframework.stereotype.Component;

@Component
public class CustomWebResponseExceptionTranslator implements WebResponseExceptionTranslator {

    private ThrowableAnalyzer throwableAnalyzer = new DefaultThrowableAnalyzer();

    public CustomWebResponseExceptionTranslator() {
    }

    @Override
    public ResponseEntity translate(Exception e) throws Exception {
        Throwable[] causeChain = this.throwableAnalyzer.determineCauseChain(e);
        OAuth2Exception ase = (OAuth2Exception)this.throwableAnalyzer.getFirstThrowableOfType(OAuth2Exception.class, causeChain);
        if (ase instanceof InvalidTokenException)
            return handleException(new TokenExpiredException());
        if (ase instanceof InvalidGrantException)
            return handleException(new InvalidGrantException());
        return handleException(ase);
    }

    private ResponseHelper handleException(OAuth2Exception e) {
        return new ResponseHelper(e.getLocalizedMessage(), null, HttpStatus.resolve(e.getHttpErrorCode()), e.getOAuth2ErrorCode());
    }

    public void setThrowableAnalyzer(ThrowableAnalyzer throwableAnalyzer) {
        this.throwableAnalyzer = throwableAnalyzer;
    }

    private static class TokenExpiredException extends OAuth2Exception {
        public TokenExpiredException() {
            super("Access token expired");
        }
    }

    private static class InvalidGrantException extends OAuth2Exception {
        public InvalidGrantException() {
            super("Bad Credentials");
        }
    }
}
