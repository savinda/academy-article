package id.investree.academy.article.exceptions;

public class DataNotFoundException extends Exception {
    public DataNotFoundException(Long id) {
        super("Data Not Found with id: " + id);
    }
    public DataNotFoundException(String id) {
        super("Data Not Found with username: " + id);
    }
}
