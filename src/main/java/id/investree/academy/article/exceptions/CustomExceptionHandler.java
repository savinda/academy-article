package id.investree.academy.article.exceptions;

import id.investree.academy.article.helper.response.ResponseHelper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.util.Objects;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseHelper handleAllExceptions(Exception ex) {
        return new ResponseHelper(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                null,
                HttpStatus.INTERNAL_SERVER_ERROR,
                ex.toString());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseHelper handleIllegalArgumentException(IllegalArgumentException ex) {
        return new ResponseHelper(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DuplicateUsernameException.class)
    public ResponseHelper handleDuplicateUsernameException(DuplicateUsernameException ex) {
        return new ResponseHelper(ex.getMessage(), null, HttpStatus.BAD_REQUEST, "");
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseHelper handleAccessDeniedException(AccessDeniedException ex) {
        return new ResponseHelper("You might be not authorized to do this request!", null, HttpStatus.FORBIDDEN, "");
    }

    @ExceptionHandler({ DataNotFoundException.class, EntityNotFoundException.class })
    public ResponseHelper handleDataNotFound(Exception ex) {
        return new ResponseHelper(ex.getLocalizedMessage(), null, HttpStatus.NOT_FOUND, "");
    }

    @ExceptionHandler( UsernameNotFoundException.class )
    public ResponseHelper handleUsernameNotFoundException(UsernameNotFoundException ex) {
        return new ResponseHelper(ex.getLocalizedMessage(), null, HttpStatus.NOT_FOUND, "");
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
                                                                         HttpHeaders headers,
                                                                         HttpStatus status,
                                                                         WebRequest request) {
        ExceptionModel model = new ExceptionModel(ex.getLocalizedMessage(), HttpStatus.METHOD_NOT_ALLOWED);
        return new ResponseEntity<>(model.response(), HttpStatus.METHOD_NOT_ALLOWED);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ExceptionModel model = new ExceptionModel(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(model.response(), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        BindingResult bindingResult = ex.getBindingResult();
        String errorMessage = Objects.requireNonNull(Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage());
        ExceptionModel model = new ExceptionModel(errorMessage, status);
        return new ResponseEntity<>(model.response(), status);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ExceptionModel model = new ExceptionModel(ex.getMessage(), status);
        return new ResponseEntity<>(model.response(), status);
    }
}
