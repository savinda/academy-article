package id.investree.academy.article.exceptions;

import id.investree.academy.article.helper.response.BaseResponse;
import org.springframework.http.HttpStatus;

public class ExceptionModel {
    private String errorMessage;
    private HttpStatus errorCode;
    private String debugInfo;

    public ExceptionModel(String errorMessage, HttpStatus errorCode) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
        this.debugInfo = "";
    }

    public BaseResponse response() {
        return new BaseResponse(errorMessage, null, errorCode, debugInfo);
    }

}
