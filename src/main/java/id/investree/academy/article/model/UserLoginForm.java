package id.investree.academy.article.model;

import javax.validation.constraints.NotNull;

public class UserLoginForm {

    @NotNull(message = "Username must be filled!")
    private String username;

    @NotNull(message = "Password must be filled")
    private String password;

    private String grantType = "password";

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getGrantType() {
        return grantType;
    }
}
