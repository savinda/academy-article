package id.investree.academy.article.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class UserRegistrationForm {
    @NotNull(message = "Username Name must be filled!")
    private String username;

    @NotNull(message = "Password Name must be filled!")
    private String password;

    @NotNull(message = "Full Name must be filled!")
    private String fullName;

    @NotNull(message = "Email must be filled!")
    @Email(message = "Not a valid email address!")
    private String email;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
