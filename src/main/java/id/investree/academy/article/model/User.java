package id.investree.academy.article.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import id.investree.academy.article.entity.PostEntity;
import id.investree.academy.article.entity.RoleEntity;

import java.util.Date;
import java.util.List;

public class User {

    private Long id;
    private String username;

    @JsonIgnore
    private String password;

    private String fullName;
    private String email;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<RoleEntity> roles;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Post> posts;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int totalPosts;

    @JsonIgnore
    private Date createdAt;

    public User(String username, String fullName) {
        this.username = username;
        this.fullName = fullName;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public List<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleEntity> roles) {
        this.roles = roles;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public int getTotalPosts() {
        return totalPosts;
    }

    public void setTotalPosts(int totalPosts) {
        this.totalPosts = totalPosts;
    }
}
