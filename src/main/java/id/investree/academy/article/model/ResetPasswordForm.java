package id.investree.academy.article.model;

import javax.validation.constraints.NotNull;

public class ResetPasswordForm {

    @NotNull(message = "Username must be filled!")
    private String username;

    @NotNull(message = "Password must be filled!")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
