package id.investree.academy.article.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.Email;

public class UserProfileForm {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Email(message = "Not a valid email address!")
    private String email;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String fullName;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
