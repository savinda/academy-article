package id.investree.academy.article.repository;

import id.investree.academy.article.entity.PostEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<PostEntity, Long>, PagingAndSortingRepository<PostEntity, Long> {
    Optional<PostEntity> findByIdAndDeletedAtIsNull(Long id);
    List<PostEntity> findByDeletedAtIsNull();
    List<PostEntity> findByDeletedAtIsNull(Pageable pageable);
}
