package id.investree.academy.article.helper.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;

public class BaseResponse {
    private String status;
    private Object data;
    private MetaResponse meta;

    public BaseResponse(String message, Object data, HttpStatus httpStatus, String debugInfo) {
        this.data = data;
        this.meta = new MetaResponse(httpStatus.value(), message, debugInfo);
        this.status = "OK";
        if (httpStatus.isError()) {
            this.status = "ERROR";
        }
    }

    public BaseResponse(Object data, HttpStatus httpStatus) {
        this.meta = new MetaResponse(httpStatus.value(), httpStatus.getReasonPhrase(), "");
        this.data = data;
        this.status = "OK";
        if (httpStatus.isError()) {
            this.status = "ERROR";
        }
    }

    public String getStatus() {
        return status;
    }

    public Object getData() {
        return data;
    }

    public MetaResponse getMeta() {
        return meta;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";
        try {
            s = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return s;
    }
}

