package id.investree.academy.article.helper.authenticated.user;

import id.investree.academy.article.entity.UserEntity;
import id.investree.academy.article.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticatedUserImpl implements AuthenticatedUser {

    @Autowired
    UserRepository userRepository;

    @Override
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public Long getUserId() {
        UserEntity currentUser = userRepository.findByUsername(getAuthentication().getName());
        return currentUser.getId();
    }
}
