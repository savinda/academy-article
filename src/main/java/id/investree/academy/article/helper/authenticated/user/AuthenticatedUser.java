package id.investree.academy.article.helper.authenticated.user;

import org.springframework.security.core.Authentication;

public interface AuthenticatedUser {
    Authentication getAuthentication();
    Long getUserId();
}
