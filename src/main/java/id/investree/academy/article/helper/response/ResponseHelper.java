package id.investree.academy.article.helper.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseHelper extends ResponseEntity {

    public ResponseHelper(Object body, HttpStatus status) {
        super(new BaseResponse(body, status), status);
    }

    public ResponseHelper(String message, Object data, HttpStatus status, String debugInfo) {
        super(new BaseResponse(message, data, status, debugInfo), status);
    }

    public ResponseHelper(HttpStatus status) {
        super(new BaseResponse(status.getReasonPhrase(), null, status, ""), status);
    }
}
