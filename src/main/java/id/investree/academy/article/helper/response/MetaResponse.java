package id.investree.academy.article.helper.response;

public class MetaResponse {
    private int code;
    private String message;
    private String debugInfo;

    public MetaResponse(int code, String message, String debugInfo) {
        this.code = code;
        this.message = message;
        this.debugInfo = debugInfo;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getDebugInfo() {
        return debugInfo;
    }
}
